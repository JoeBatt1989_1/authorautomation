<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>signOutBtn</name>
   <tag></tag>
   <elementGuidId>d98ff699-103a-4ef0-a3f1-56dabd7643d5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>.coral-Shell-user .coral-Shell-user-footer a:nth-child(2)</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
