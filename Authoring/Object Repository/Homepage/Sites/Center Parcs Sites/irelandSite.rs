<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>irelandSite</name>
   <tag></tag>
   <elementGuidId>fce41a09-30f7-4758-a376-a77c5954adae</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@title = 'Ireland']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>[data-foundation-layout-columnview-columnid='/content/centerparcs'] [title='Ireland']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Ireland</value>
   </webElementProperties>
</WebElementEntity>
