<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ukSite</name>
   <tag></tag>
   <elementGuidId>22f3e01a-5d7a-4143-b542-4ae0f2ee991b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@title = 'UK']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>[data-foundation-layout-columnview-columnid='/content/centerparcs'] [title='UK']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>UK</value>
   </webElementProperties>
</WebElementEntity>
