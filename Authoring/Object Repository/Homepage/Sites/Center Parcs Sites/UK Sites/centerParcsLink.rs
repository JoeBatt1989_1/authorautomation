<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>centerParcsLink</name>
   <tag></tag>
   <elementGuidId>f50e8d4d-fe13-481d-a272-21af887e83ff</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>CSS</key>
         <value>[data-foundation-layout-columnview-columnid='/content/centerparcs/uk'] [title='Center Parcs']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
