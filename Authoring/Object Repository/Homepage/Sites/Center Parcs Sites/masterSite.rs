<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>masterSite</name>
   <tag></tag>
   <elementGuidId>7b6539a9-fdf5-4077-b5bc-a17fb39d595b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@title = 'Master']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>[data-foundation-layout-columnview-columnid='/content/centerparcs'] [title='Master']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Master</value>
   </webElementProperties>
</WebElementEntity>
