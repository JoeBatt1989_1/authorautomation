<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>centerParcsSite</name>
   <tag></tag>
   <elementGuidId>b7fec931-a0bd-4471-86df-766650e35b35</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@title = 'Center Parcs']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>[data-foundation-layout-columnview-columnid='/content'] [title='Center Parcs']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Center Parcs</value>
   </webElementProperties>
</WebElementEntity>
