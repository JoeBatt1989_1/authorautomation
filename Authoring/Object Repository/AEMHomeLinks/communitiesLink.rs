<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>communitiesLink</name>
   <tag></tag>
   <elementGuidId>10e1a20d-cb41-430e-84ee-9eabab46a74b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>.coral-Shell-menu--full.is-open coral-masonry-item:nth-child(10) div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
