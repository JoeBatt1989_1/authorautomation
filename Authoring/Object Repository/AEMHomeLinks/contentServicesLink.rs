<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>contentServicesLink</name>
   <tag></tag>
   <elementGuidId>16d53945-ce76-4566-a1d8-3a1c8894abdf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>.coral-Shell-menu--full.is-open coral-masonry-item:nth-child(5) div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
