<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>projectsLink</name>
   <tag></tag>
   <elementGuidId>b63ee418-0b18-43ad-8962-efad7b976425</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>.coral-Shell-menu--full.is-open coral-masonry-item:nth-child(1) div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
