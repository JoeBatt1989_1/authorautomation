<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>personalizationLink</name>
   <tag></tag>
   <elementGuidId>cb16e2df-199e-48e4-a628-b57a5b509947</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>.coral-Shell-menu--full.is-open coral-masonry-item:nth-child(8) div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
