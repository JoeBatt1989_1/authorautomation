import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Helper Methods/Sites_Navigation/e2e_Flows/NavToUKSpecialOffers'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Helper Methods/Sites_Navigation/e2e_Flows/NavToIESpecialOffers'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Helper Methods/Sites_Navigation/e2e_Flows/NavToMasterWhatsIncluded'), [:], FailureHandling.STOP_ON_FAILURE)

GlobalVariable.pickerItemText = '/content/centerparcs/master/en/discover-center-parcs/short-family-breaks'

WebUI.callTestCase(findTestCase('Helper Methods/Page_Actions/TickPage'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Helper Methods/Page_Actions/Actions_Toolbar/SelectEditButton'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Helper Methods/Page_Editor/SelectContentForEditing'), [:], FailureHandling.STOP_ON_FAILURE)

sleep(1000)

