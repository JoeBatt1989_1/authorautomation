import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.waitForElementVisible(findTestObject('Action Bar/editBtn'), 0)

WebUI.click(findTestObject('Action Bar/editBtn'))

WebDriver driver = DriverFactory.getWebDriver()

Set<String> handles = driver.getWindowHandles()

attempts = 0

while (handles.size() < 2) {
    attempts++

    if (attempts == 10) {
        println('Additional tab was not opened')

        break
    }
    
    sleep(500)

    handles = driver.getWindowHandles()
}

driver.switchTo().window(handles[1])

Boolean editorDisplayed = false 
attempts = 0

while(editorDisplayed == false){
	attempts++
	try {
		WebUI.waitForElementVisible(findTestObject('Page Editor/Action Bar/actionBar'), 0)
		editorDisplayed = true
	} catch (Exception e) {
		if(attempts == 10){
			println ("Page in edit mode not displayed")
			throw e
		}
		sleep(500)
	}
}

