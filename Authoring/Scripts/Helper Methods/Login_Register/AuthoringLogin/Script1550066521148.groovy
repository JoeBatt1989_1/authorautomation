import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('https://uatauthor.centerparcs.co.uk/')

WebUI.maximizeWindow()

WebUI.waitForElementVisible(findTestObject('Login Page/userName'), 0)

WebUI.sendKeys(findTestObject('Login Page/userName'), 'joe.batt@centerparcs.co.uk')

WebUI.sendKeys(findTestObject('Login Page/password'), 'Password1')

WebUI.waitForElementClickable(findTestObject('Login Page/signInBtn'), 0)

WebUI.click(findTestObject('Login Page/signInBtn'))

WebUI.waitForElementVisible(findTestObject('Homepage/userMenuIcon'), 0)

WebUI.callTestCase(findTestCase('Helper Methods/Login_Register/ClosePopUps'), [:], FailureHandling.STOP_ON_FAILURE)

