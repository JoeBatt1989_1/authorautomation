import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.waitForElementPresent(findTestObject('Page Editor/Center Parcs Experience/shortFamilyHolidaysText'), 0)

WebUI.scrollToElement(findTestObject('Page Editor/Center Parcs Experience/shortFamilyHolidaysText'), 0)

WebUI.click(findTestObject('Page Editor/Center Parcs Experience/shortFamilyHolidaysText'))

WebUI.waitForElementVisible(findTestObject('Page Editor/Editor Toolbars/Content Editor Toolbar/contentToolbar'), 0)

WebUI.waitForElementVisible(findTestObject('Page Editor/Editor Toolbars/Content Editor Toolbar/Toolbar Buttons/editBtn'), 0)

WebUI.click(findTestObject('Action Bar/editBtn'))

WebUI.waitForElementVisible(findTestObject('Page Editor/Editor Toolbars/Text Editor Toolbar/textToolbar'), 0)

